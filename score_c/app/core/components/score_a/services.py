import json
import os

import requests


class Service:
    @staticmethod
    def run(data):
        config = Service.get_config()
        data = {
            "input": 2
        }
        headers = {
            'Content-Type': 'application/json'
        }
        response = requests.post(
            config['url'],
            data=json.dumps(data),
            headers=headers
        )
        return response.text, response.status_code

    @staticmethod
    def get_config():
        data = open(f'{os.path.dirname(__file__)}/config.json', "r")
        config = json.load(data)
        data.close()
        return config
