from .process import RulesProcess
from .process import PredictProcess


class ScoreHandler:
    @staticmethod
    def predict(data):
        rule = RulesProcess(data["product"])
        score = PredictProcess(data, rule)
        return score.predict()
