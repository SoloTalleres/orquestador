RULES = {
    "3eedcc40-f04e-11ea-adc1-0242ac120002": {
        'uuid': '96cb35f8-85cb-11ea-8f78-d5e85ffb991c',
        'product': 'my_fintech',
        'max_process': 3,
        'version': 1.1,
        'steps': {
            'score_a': {
                'active': False,
                'order': 1,
                'skip': False,
                'editable': False,
                'status': ['analysis', 'rejected'],
                'verify_application': True,
            },
            'score_b': {
                'active': False,
                'order': 1,
                'skip': False,
                'editable': False,
                'status': ['analysis', 'rejected'],
                'verify_application': True,
            }
        }
    }
}
