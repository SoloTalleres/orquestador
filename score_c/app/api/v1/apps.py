from django.apps import AppConfig


class V1Config(AppConfig):
    name = 'app.api.v1'
