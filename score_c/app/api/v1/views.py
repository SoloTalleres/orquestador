import json

from django.http import JsonResponse
from django.views import View
from app.core.handlers import ScoreHandler


class RootViews(View):
    def get(self, request):
        output = {
            "message": "Microservice Score C"
        }
        return JsonResponse(
            output
        )


class PredictViews(View):
    def post(self, request, *args, **kwargs):
        score = ScoreHandler.predict(
            request.POST
        )
        output = {
                "score": score,
                "service": "Score C"
            }
        return JsonResponse(
            output
        )
