from pydantic import BaseModel


class InScoreSerializer(BaseModel):
    input: str


class OutScoreSerializer(BaseModel):
    score: str
    service: str


class SuccessSerializer(BaseModel):
    message: str
