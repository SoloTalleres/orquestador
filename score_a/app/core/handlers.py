from .process import PredictProcess


class ScoreHandler:
    @staticmethod
    def predict(data):
        score = PredictProcess(data)
        return score.predict()
