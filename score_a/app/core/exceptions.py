from app.core.contants import ERROR_LIMIT_PROCESS


class LimitCreditRequestException(Exception):
    def message(self):
        return ERROR_LIMIT_PROCESS

