# ERRORS
ERROR_LIMIT_PROCESS = {
    "message": "Limit of credits per user",
    "code": 1001
}


# CONFIG
INITIAL_STEP_STATUS = "EMPTY"
COMPLETED_STATUS = "COMPLETED"
INITIAL_CREDIT_REQUEST_STATUS = "REGISTERED"

CREDIT_REQUEST_STATUS = (
    ('EMPTY', 'Empty'),
    ('PENDING', 'Pending'),
    ('COMPLETED', 'Completed')
)

STEP_STATUS = (
    ('EMPTY', 'Empty'),
    ('PENDING', 'Pending'),
    ('COMPLETED', 'Completed')
)