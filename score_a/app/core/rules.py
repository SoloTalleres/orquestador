RULES = {
    "3eedcc40-f04e-11ea-adc1-0242ac120002": {
        'uuid': '96cb35f8-85cb-11ea-8f78-d5e85ffb991c',
        'product': 'aliatu_convenio_uno',
        'sequential': False,
        'partial': False,
        'max_process': 3,
        'version': 1.1,
        'status': {
            'analysis': 'Analysis',
            'pre-approved': 'Pre-approved',
            'approved': 'Approved',
            'rejected': 'Rejected'
        },
        'steps': {
            'calculadora': {
                'type': 'user',
                'active': False,
                'order': 1,
                'skip': False,
                'dependencies': [],
                'editable': False,
                'status': ['analysis', 'rejected'],
                'verify_application': True,
                'actions': {
                    'reserved': {
                        'entry': {
                            'notify': {
                                'clever': {
                                    'url': '',
                                    'method': 'get',
                                    'params': [
                                        'param1',
                                        'param2',
                                    ]
                                }
                            }
                        },
                        'do': {
                            'notify': {
                                'clever': {}
                            },
                        },
                        'exit': {
                            'notify': {
                                'clever': {
                                    'url': '',
                                    'method': 'get',
                                    'params': [
                                        'param1',
                                        'param2',
                                    ]
                                }
                            }
                        },
                        'on_failure': {  # could be just retry_on_failure: Bool ?
                            'notify': {
                                'clever': {},
                                'logger': {},
                                'client': {}
                            },
                            'try_now': {
                                'max_retries': 3
                            },
                            'try_later': {
                                'reconciliation': 'file/path/or/db/with/data/to/try/later/or/check'
                            },
                            'show_error': {
                                'user': {}
                            },
                        }
                    },
                    'public': {
                        'user': {
                            'cancel': False,
                            'resettable': False
                        },
                        'admin': {
                            'cancel': False,
                            'delete': False,
                            'approved': False,
                            'verify': False,
                            'resettable': False
                        },
                    }
                },
                'transition': {
                    'after': [
                        'dynamic_forms'
                    ],
                    'before': []
                }
            },
            'dynamic_forms': {
                'type': 'user',
                'active': True,
                'order': 2,
                'skip': False,
                'dependencies': [
                    'new_request'
                ],
                'editable': False,
                'verify_application': True,
                'status': False,
                'actions': {
                    'reserved': {
                        'entry': {
                            'notify': {
                                'clever': {
                                    'url': '',
                                    'method': 'get',
                                    'params': [
                                        'param1',
                                        'param2',
                                    ]
                                }
                            }
                        },
                        'do': {
                            'notify': {
                                'clever': {}
                            },
                        },
                        'exit': {
                            'notify': {
                                'clever': {
                                    'url': '',
                                    'method': 'get',
                                    'params': [
                                        'param1',
                                        'param2',
                                    ]
                                }
                            }
                        },
                    },
                    'public': {
                        'user': {
                            'cancel': False,
                            'resettable': False
                        },
                        'admin': {
                            'cancel': False,
                            'delete': False,
                            'approved': False,
                            'verify': False,
                            'resettable': False
                        },
                    }
                },
                'transition': {
                    'after': [
                        'signature'
                    ],
                    'before': False
                }
            },
            'admin_validate': {
                'type': 'admin',
                'active': True,
                'order': 2,
                'skip': False,
                'depend': [
                    'new_request'
                ],
                'editable': False,
                'verify_application': True,
                'actions': {
                    'reserved': {
                        'entry': {
                            'notify': {
                                'clever': {
                                    'url': '',
                                    'method': 'get',
                                    'params': [
                                        'param1',
                                        'param2',
                                    ]
                                }
                            }
                        },
                        'do': {
                            'notify': {
                                'clever': {}
                            },
                        },
                        'exit': {
                            'notify': {
                                'clever': {
                                    'url': '',
                                    'method': 'get',
                                    'params': [
                                        'param1',
                                        'param2',
                                    ]
                                }
                            }
                        },
                    },
                    'public': {
                        'user': {
                            'cancel': False,
                            'resettable': False
                        },
                        'admin': {
                            'cancel': False,
                            'delete': False,
                            'approved': False,
                            'verify': False,
                            'resettable': False
                        },
                    }
                },
                'transition': {
                    'after': [
                        'signature'
                    ],
                    'before': False
                }
            },
            'signature': {
                'type': 'admin',
                'active': True,
                'order': 2,
                'skip': False,
                'depend': [
                    'new_request'
                ],
                'editable': False,
                'verify_application': True,
                'actions': {
                    'reserved': {
                        'entry': {
                            'notify': {
                                'clever': {
                                    'url': '',
                                    'method': 'get',
                                    'params': [
                                        'param1',
                                        'param2',
                                    ]
                                }
                            }
                        },
                        'do': {
                            'notify': {
                                'clever': {}
                            },
                        },
                        'exit': {
                            'notify': {
                                'clever': {
                                    'url': '',
                                    'method': 'get',
                                    'params': [
                                        'param1',
                                        'param2',
                                    ]
                                }
                            }
                        },
                    },
                    'public': {
                        'user': {
                            'cancel': False,
                            'resettable': False
                        },
                        'admin': {
                            'cancel': False,
                            'delete': False,
                            'approved': False,
                            'verify': False,
                            'resettable': False
                        },
                    }
                },
                'transition': {
                    'after': [
                        'signature2'
                    ],
                    'before': False
                }
            },
            'validate': {
                'type': 'admin',
                'active': True,
                'order': 1,
                'skip': False,
                'depend': [
                    'new_request'
                ],
                'editable': False,
                'verify_application': True,
                'actions': {
                    'reserved': {
                        'entry': {
                            'notify': {
                                'clever': {
                                    'url': '',
                                    'method': 'get',
                                    'params': [
                                        'param1',
                                        'param2',
                                    ]
                                }
                            }
                        },
                        'do': {
                            'notify': {
                                'clever': {}
                            },
                        },
                        'exit': {
                            'notify': {
                                'clever': {
                                    'url': '',
                                    'method': 'get',
                                    'params': [
                                        'param1',
                                        'param2',
                                    ]
                                }
                            }
                        },
                    },
                    'public': {
                        'user': {
                            'cancel': False,
                            'resettable': False
                        },
                        'admin': {
                            'cancel': False,
                            'delete': False,
                            'approved': False,
                            'verify': False,
                            'resettable': False
                        },
                    }
                },
                'transition': {
                    'after': [
                        'signature'
                    ],
                    'before': False
                }
            },
        }
    }
}
