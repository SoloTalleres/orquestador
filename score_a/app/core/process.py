from random import random

import boto3

from app.core.helpers import publish
from .queryset import ScoreQuerySet


class PredictProcess:
    def __init__(self, data):
        self.data = data
        self.query_set = ScoreQuerySet()

    def predict(self):
        machine_learning_result = random()
        self.query_set.saved(machine_learning_result)
        publish(machine_learning_result)
        print("PROCESO TERMINADO SERVICIO A")
        return machine_learning_result


