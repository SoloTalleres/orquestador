import falcon
from spectree import SpecTree

from config import settings
from config.openapi import PAGES
from config.openapi import PATH
from config.openapi import TITLE
from config.openapi import VERSION

"""
Initialize falcon
"""
app = falcon.API()

"""
Initialize OpenApi
"""
api = SpecTree(
    'falcon',
    title=TITLE,
    version=VERSION,
    path=PATH,
    page=PAGES["swagger"]
)

"""
Initialize URL
"""
from app.api.v1 import urls



