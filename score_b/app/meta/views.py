from fastapi import APIRouter
from starlette import status
from starlette.responses import JSONResponse
from config.settings import API_VERSION


router = APIRouter()


@router.get("/", tags=["meta"])
async def root():
    return {"MicroService": "Score B"}


@router.get("/health", tags=["meta"])
async def health_check():
    response = {"status": "ok"}
    return JSONResponse(
        status_code=status.HTTP_200_OK,
        content=response
    )
