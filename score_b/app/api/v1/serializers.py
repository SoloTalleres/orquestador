from pydantic import BaseModel


class InScoreSerializer(BaseModel):
    input: str


class OutScoreSerializer(BaseModel):
    output: str


class SuccessSerializer(BaseModel):
    message: str
