from typing import Any
from typing import Dict

from fastapi import APIRouter
from app.core.handlers import ScoreHandler
from .serializers import InScoreSerializer


router = APIRouter()


@router.post("/predict", tags=["Score Process"])
async def get_score(data: InScoreSerializer):
    score = ScoreHandler.predict(data)
    return score


@router.get("/predict", tags=["Score Confirm"])
async def confirm_score():
    output = {
        'score': 1,
        'name': 'score_b'
    }
    return output


@router.post("/listener", tags=["Score Confirm"])
async def confirm_score(data: Dict[Any, Any] = None):
    print(data['Message'])
    return data['Message']