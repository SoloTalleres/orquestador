import os
from celery import Celery


app = Celery(
    'tasks',
    backend=os.getenv("CELERY_RESULT_BACKEND"),
    broker=os.getenv("CELERY_BROKER_URL"),
    include=['app.core.tasks'],
    namespace='CELERY'
)

app.conf.timezone = os.environ.get('TIME_ZONE')
app.conf.enable_utc = True


@app.task(bind=True)
def debug_task(self):
    print('Request: {0!r}'.format(self.request))
