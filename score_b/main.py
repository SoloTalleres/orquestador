from fastapi import FastAPI
from config import routers
from config import settings

app = FastAPI(title="Score", redoc_url="/redoc")

app.include_router(
    routers.urls
)
