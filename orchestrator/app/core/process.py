from random import random

from app.core.base import Factory
from app.core.rules import RULES
from .contants import COMPONENTS
from .queryset import ScoreQuerySet


class RulesProcess:
    def __init__(self, rules_id):
        self.rules_id = rules_id
        self.rules = self.get_rules()

    def get_rules(self):
        rule = {}
        try:
            rule = RULES[str(self.rules_id)]
        except Exception as e:
            pass
        return rule

    @property
    def count_steps(self):
        return len(self.rules['steps'])

    @property
    def count_services(self):
        return len(self.rules['services'])

    @property
    def services(self):
        return self.rules['services']

    @property
    def validate_data(self):
        return self.rules['validate_data']

    @property
    def steps(self):
        return self.rules['steps']

    @property
    def calculate_amount(self):
        return self.rules['calculate_amount']

    @property
    def all_sync(self):
        return self.rules['all_sync']

    @property
    def sort_rules(self):
        _ = sorted(
            self.rules['steps'],
            key=lambda name: self.rules['steps'][name]['order']
        )
        return _


class PredictProcess:
    def __init__(self, data, rule):
        self.data = data
        self.rule = rule
        self.query_set = ScoreQuerySet()

    def predict(self):
        for rule in self.rule.sort_rules:
            self.get_factory(rule)
        machine_learning_result = random()
        self.query_set.saved(machine_learning_result)
        return machine_learning_result

    def get_factory(self, component):
        instance = Factory(
            "Service",
            f'{COMPONENTS}.{component}.services'
        ).get_instance()
        print(f'*********** INSTANCIA: {component}****************')
        print(instance)
        return instance.run(self.data)
