from .exceptions import FactoryNotInstanceAndPackage


class Factory:
    def __init__(self, instance=None, base=None):
            self.base = base
            self.instance = instance

    def get_instance(self):
        if not self.instance:
            raise FactoryNotInstanceAndPackage.message

        parts = f"{self.base}.{self.instance}".split('.')
        module_name = ".".join(parts[:-1])
        module = __import__(module_name)
        for comp in parts[1:]:
            module = getattr(module, comp)
        return module

