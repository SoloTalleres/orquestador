from pydantic import BaseModel


class InScoreSerializer(BaseModel):
    product: str
    document: str
    plus: str


class OutScoreSerializer(BaseModel):
    score: str
    service: str


class SuccessSerializer(BaseModel):
    message: str
