from .views import HealthViews
from .views import RootViews
from .views import PredictViews
from .views import ListenerViews
from config import api
from config import app

app.add_route(
    '/',
    RootViews()
)

app.add_route(
    '/api/v1/predict',
    PredictViews()
)

app.add_route(
    '/api/v1/listener',
    ListenerViews()
)

app.add_route(
    '/health',
    HealthViews()
)

api.register(app)
