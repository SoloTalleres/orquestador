import json

import falcon
from spectree import Response

from app.core.handlers import ScoreHandler
from config import api
from .serializaers import InScoreSerializer
from .serializaers import OutScoreSerializer
from .serializaers import SuccessSerializer


class RootViews:
    @api.validate(
        resp=Response(
            HTTP_200=SuccessSerializer,
            HTTP_403=None
        ), tags=['Root']
    )
    def on_get(self, request, response, **kwargs):
        """

        :param request:
        :param response:
        :param kwargs:
        :return:
        """
        output = {
            'message': 'Microservice Orchestrator'
        }
        response.status = falcon.HTTP_200
        response.media = output


class PredictViews:
    @api.validate(
        resp=Response(
            HTTP_201=None,
            HTTP_200=None,
            HTTP_400=None
        ), tags=['Init Process Credit Request']
    )
    def on_get(self, request, response):
        try:
            output = {
                'document': 1,
                'plus': True
            }
            response.status = falcon.HTTP_200
            response.media = output
        except Exception as e:
            output = {
                'message': str(e),
                'code': 1001
            }
            response.status = falcon.HTTP_400
            response.media = output

    @api.validate(
        json=InScoreSerializer,
        resp=Response(
            HTTP_201=None,
            HTTP_200=None,
            HTTP_400=None
        ), tags=['Init Process Credit Request']
    )
    def on_post(self, request, response):
        score = ScoreHandler.predict(
            request.media
        )
        try:
            response.status = falcon.HTTP_200
            response.media = {
                "score": '1',
                "service": "Orchestrator"
            }
        except Exception as e:
            print(e)
            output = {
                'message': str(e),
                'code': 1001
            }
            response.status = falcon.HTTP_400
            response.media = output


class ListenerViews:
    @api.validate(
        resp=Response(
            HTTP_200=None,
            HTTP_403=None
        ), tags=['Meta']
    )
    def on_post(self, request, response):
        print(request.media)
        print("*************************************")
        output = {
            "status": "ok..."
        }
        response.body = json.dumps(output)
        response.status = falcon.HTTP_200


class HealthViews:
    @api.validate(
        resp=Response(
            HTTP_200=None,
            HTTP_403=None
        ), tags=['Meta']
    )
    def on_get(self, request, response):
        output = {
            "status": "ok"
        }
        response.body = json.dumps(output)
        response.status = falcon.HTTP_200
