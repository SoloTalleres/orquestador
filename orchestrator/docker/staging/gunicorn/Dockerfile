FROM zinobe/python37

ARG SERVICE
ARG STAGE

ENV SERVICE=${SERVICE}
ENV STAGE=${STAGE}

RUN apt-get update \
  && apt-get install -y gcc python3-dev \
    libffi-dev \
    poppler-utils \
    libglib2.0-0 \
    libsm6 libxext6 libxrender-dev

WORKDIR /www

COPY . /www

COPY docker/${STAGE}/gunicorn/supervisord.conf /etc/supervisor/conf.d/gunicorn.conf

COPY docker/${STAGE}/gunicorn/entrypoint /entrypoint

# Load .env
RUN AWS_ENV_PATH=/ecs/${SERVICE}/${STAGE}/config/ AWS_REGION=us-west-2 aws-env --format dotenv > .env

# Remove quotes from .envc
RUN sed -i 's/"//g' .env

# install requirements
RUN apt-get update -y
RUN apt-get upgrade -y
RUN pip install --upgrade pip
RUN pip install --no-cache-dir -r /www/requirements.txt
RUN pip3 install 'pymongo[srv]'

ENTRYPOINT ["/entrypoint"]
