import falcon
from falcon import media
from falcon.media import JSONHandler
from spectree import SpecTree

from config import settings
from config.openapi import PAGES
from config.openapi import PATH
from config.openapi import TITLE
from config.openapi import VERSION

"""
Initialize falcon
"""
app = falcon.API(
    middleware=settings.MIDDLEWARES
)


class XMLHandler(media.BaseHandler):

    def deserialize(self, stream, content_type, content_length):
        return str(stream.read())

    def serialize(self, media, content_type):
        raise NotImplementedError


app.req_options.media_handlers = media.Handlers({
    'text/plain': XMLHandler(),
    'text/html': XMLHandler(),
    'application/json': JSONHandler(),
})

"""
Initialize OpenApi
"""
api = SpecTree(
    'falcon',
    title=TITLE,
    version=VERSION,
    path=PATH,
    page=PAGES["swagger"]
)

"""
Initialize URL
"""

from app.api.v1 import urls



